package com.rest.project.dto.rest;

import com.rest.project.domain.Image;
import com.rest.project.domain.Product;

public class ImageResource extends Resource {

	private Long id;
	
	private String type;
	
	private ProductResource productResource;
	
	public ImageResource(){}
	
	public ImageResource(Image image) throws Exception {

		if(image != null) {
			
			this.setId(image.getId());
			this.setType(image.getType());
			
			if(image.getProduct() != null && image.getProduct().getId() != null) {
				this.setProduct(new ProductResource(image.getProduct()));				
			}
		}
	}

	public ImageResource(Long id){
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ProductResource getProduct() {
		return productResource;
	}

	public void setProduct(ProductResource productResource) {
		this.productResource = productResource;
	}

	@Override
	public String toString() {
		return "ImageResource [id=" + id + ", type=" + type + ", product=" + productResource + "]";
	}

	public static ImageResource fromEntity(Image image) throws Exception {
		
		ImageResource resource = null;
		
		if(image != null) {
			
			resource = new ImageResource();
			
			resource.setId(image.getId());
			resource.setType(image.getType());
			resource.setProduct(new ProductResource(image.getProduct()));
			
		}
		
		return resource;
	}

	public static Image toEntity(ImageResource imageResource) throws Exception {
		
		Image entity = null;
		
		if(imageResource != null) {
			
			entity = new Image();
			
			entity.setId(imageResource.getId());
			entity.setType(imageResource.getType());
			
			if(imageResource.getProduct() != null) {
				entity.setProduct(new Product(imageResource.getProduct().getId()));
			}
		}
		
		return entity;
	}

}
