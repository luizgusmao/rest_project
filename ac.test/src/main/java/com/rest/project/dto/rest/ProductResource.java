package com.rest.project.dto.rest;

import com.rest.project.domain.Product;

public class ProductResource extends Resource {

	private Long id;
	
	private String name;
	
	private String description;
	
	private ProductResource parentProduct;
	
	public ProductResource(){}
	
	public ProductResource(Product product) throws Exception {

		if(product != null) {
			
			this.setId(product.getId());
			this.setName(product.getName());
			this.setDescription(product.getDescription());
		}
	}

	public ProductResource(Long id){
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ProductResource getParentProduct() {
		return parentProduct;
	}

	public void setParentProduct(ProductResource parentProduct) {
		this.parentProduct = parentProduct;
	}

	@Override
	public String toString() {
		return "ProductResource [id=" + id + ", name=" + name + ", description=" + description + ", parentProduct="
				+ parentProduct + "]";
	}

	public static ProductResource fromEntity(Product product) throws Exception {
		
		ProductResource resource = null;
		
		if(product != null) {
			
			resource = new ProductResource();
			
			resource.setId(product.getId());
			resource.setName(product.getName());
			resource.setDescription(product.getDescription());
			resource.setParentProduct(new ProductResource(product.getParentProduct()));
		}
		
		return resource;
	}
	
	public static Product toEntity(ProductResource productResource) throws Exception {
		
		Product entity = null;
		
		if(productResource != null) {
			
			entity = new Product();
			
			entity.setId(productResource.getId());
			entity.setName(productResource.getName());
			entity.setDescription(productResource.getDescription());
			
			if(productResource.getParentProduct() != null) {
				entity.setParentProduct(new Product(productResource.getParentProduct().getId()));				
			}
		}
		
		return entity;
	}

}
