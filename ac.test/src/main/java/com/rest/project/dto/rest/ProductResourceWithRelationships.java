package com.rest.project.dto.rest;

import java.util.List;

import com.rest.project.domain.Image;
import com.rest.project.domain.Product;
import com.rest.project.util.Util;

public class ProductResourceWithRelationships extends Resource {

	private Long id;
	
	private String name;
	
	private String description;
	
	private List<ImageResource> images;
	
	private ProductResourceWithRelationships parentProduct;
	
	private List<ProductResourceWithRelationships> childProducts;
	
	public ProductResourceWithRelationships(){}
	
	public ProductResourceWithRelationships(Product product, List<ImageResource> images, List<ProductResourceWithRelationships> childrenProducts) throws Exception {

		if(product != null) {
			
			this.setId(product.getId());
			this.setName(product.getName());
			this.setDescription(product.getDescription());
			this.setParentProduct(new ProductResourceWithRelationships(product.getParentProduct(), null, null));
			
			if(images != null) {
				this.setImages(images);
			}
			
			if(childrenProducts != null) {
				this.setChildProducts(childrenProducts);
			}
			
		}
	}

	public ProductResourceWithRelationships(Long id){
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<ImageResource> getImages() {
		return images;
	}

	public void setImages(List<ImageResource> images) {
		this.images = images;
	}

	public ProductResourceWithRelationships getParentProduct() {
		return parentProduct;
	}

	public void setParentProduct(ProductResourceWithRelationships parentProduct) {
		this.parentProduct = parentProduct;
	}

	public List<ProductResourceWithRelationships> getChildProducts() {
		return childProducts;
	}

	public void setChildProducts(List<ProductResourceWithRelationships> childProducts) {
		this.childProducts = childProducts;
	}

	@Override
	public String toString() {
		return "ProductResourceWithRelationships [id=" + id + ", name=" + name + ", description=" + description
				+ ", images=" + images + ", parentProduct=" + parentProduct + ", childProducts=" + childProducts + "]";
	}

	public static ProductResourceWithRelationships fromEntity(Product product) throws Exception {
		
		return fromEntity(product, true, true);
	}

	@SuppressWarnings("unchecked")
	public static ProductResourceWithRelationships fromEntity(Product product, boolean includeImages, boolean includeChildProducts) throws Exception {
		
		ProductResourceWithRelationships resource = null;
		
		if(product != null) {
			
			resource = new ProductResourceWithRelationships();
			
			resource.setId(product.getId());
			resource.setName(product.getName());
			resource.setDescription(product.getDescription());
			resource.setParentProduct(new ProductResourceWithRelationships(product.getParentProduct(), null, null));
			
			if(includeImages) {
				resource.setImages((List<ImageResource>) Util.convertEntityListIntoResourceList(product.getImages(), ImageResource.class, Image.class));				
			}
			
			if(includeChildProducts) {
				resource.setChildProducts((List<ProductResourceWithRelationships>) Util.convertEntityListIntoResourceList(product.getChildProducts(), ProductResourceWithRelationships.class, Product.class));			
			}
			
		}
		
		return resource;
	}
	
	@SuppressWarnings("unchecked")
	public static Product toEntity(ProductResourceWithRelationships productResource) throws Exception {
		
		Product entity = null;
		
		if(productResource != null) {
			
			entity = new Product();
			
			entity.setId(productResource.getId());
			entity.setName(productResource.getName());
			entity.setDescription(productResource.getDescription());
			
			if(productResource.getParentProduct() != null) {
				entity.setParentProduct(new Product(productResource.getParentProduct().getId()));				
			}
			
			entity.setImages((List<Image>) Util.convertResourceListIntoEntityList(productResource.getImages(), ImageResource.class, Image.class ));
			entity.setChildProducts((List<Product>) Util.convertResourceListIntoEntityList(productResource.getChildProducts(), ProductResourceWithRelationships.class, Product.class));
		}
		
		return entity;
	}

}
