package com.rest.project.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rest.project.domain.Product;
import com.rest.project.dto.rest.ProductResource;
import com.rest.project.dto.rest.ProductResourceWithRelationships;
import com.rest.project.service.ProductService;
import com.rest.project.util.Util;

import io.swagger.annotations.ApiOperation;

@RestController
@Transactional(rollbackFor = Exception.class)
public class ProductController {

	@Autowired
	private ProductService productService;

	@PostMapping("/product")
	@ApiOperation(tags = { "Products" }, value = "Create product", notes = "Create a product.")
	public ResponseEntity<ProductResource> createProduct(@RequestBody ProductResource resource) throws Exception {
	
		List<Product> entities = (List<Product>) this.productService.findByNameIgnoringCase(resource.getName());
	
		if(resource.getId() != null) {
			throw new Exception("To create a product, the ID must be empty.");
		}
		
		if(entities != null && ! entities.isEmpty()) {
			throw new Exception("Already exist a product with this Name");
		}

		Product parentProduct = null;
		
		if(resource.getParentProduct() != null && resource.getParentProduct().getId() != null) {
			parentProduct = (Product) this.productService.find(Product.class, resource.getParentProduct().getId());
		}
		
		Product entity = new Product(resource.getName(), resource.getDescription(), parentProduct);
		
		this.productService.update(entity);
		
		return new ResponseEntity<ProductResource>(new ProductResource(entity), HttpStatus.OK); 
	
	}
	
	@GetMapping("/products/{id}")
	@ApiOperation(tags = { "Products" }, value = "Get Product", notes = "Get a product by it's id")
	@Transactional(readOnly = true)
	public ResponseEntity<ProductResourceWithRelationships> getProduct(@RequestParam(required = false) Long id, 
			@RequestParam(required = false, defaultValue = "true") boolean includeImages, @RequestParam(required = false, defaultValue = "true") Boolean includeChildProducts) throws Exception {

		Product entity = (Product) this.productService.find(Product.class, id);
		
		if (entity == null || entity.getId() == null) {
			throw new Exception("No product found with the id '"+id+"'");
		}

		ProductResourceWithRelationships productResourceWithRelationships = ProductResourceWithRelationships.fromEntity(entity, includeImages, includeChildProducts);

		return new ResponseEntity<ProductResourceWithRelationships>(productResourceWithRelationships, HttpStatus.OK);
	}
	
	@GetMapping("/products/{id}/products")
	@ApiOperation(tags = { "Products" }, value = "Product child list", notes = "Get a list of all child products and relationships from a product. Relations: Parent Product, child products and Images")
	@Transactional(readOnly = true)
	public ResponseEntity<List<ProductResourceWithRelationships>> listChildProductsFromProduct(@RequestParam(required = false) Long id, 
			@RequestParam(required = false, defaultValue = "true") boolean includeImages, @RequestParam(required = false, defaultValue = "true") Boolean includeChildProducts,
			Pageable pageable) throws Exception {

		Product entity = (Product) this.productService.find(Product.class, id);
		
		if (entity == null || entity.getId() == null) {
			throw new Exception("No product found with the id '"+id+"'");
		}

		List<ProductResourceWithRelationships> productResourceList = new ArrayList<ProductResourceWithRelationships>();
		
		for (Product product : entity.getChildProducts()) {
			ProductResourceWithRelationships productResourceWithRelationships = ProductResourceWithRelationships.fromEntity(product, includeImages, includeChildProducts);
			
			productResourceList.add(productResourceWithRelationships);
		}

		return new ResponseEntity<List<ProductResourceWithRelationships>>(productResourceList, HttpStatus.OK);
	}

	@GetMapping("/products")
	@ApiOperation(tags = { "Products" }, value = "Product list", notes = "Get a list of all products and relationships. Relations: Parent Product, child products and Images")
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public ResponseEntity<List<ProductResourceWithRelationships>> listProducts(@RequestParam(required = false) String name, 
			@RequestParam(required = false, defaultValue = "true") boolean includeImages, @RequestParam(required = false, defaultValue = "true") Boolean includeChildProducts,
			Pageable pageable) throws Exception {

		List<Product> entities = (List<Product>) this.productService.list(name, pageable);

		List<ProductResourceWithRelationships> productResourceList = new ArrayList<ProductResourceWithRelationships>();
		
		for (Product product : entities) {
			ProductResourceWithRelationships productResourceWithRelationships = ProductResourceWithRelationships.fromEntity(product, includeImages, includeChildProducts);
			
			productResourceList.add(productResourceWithRelationships);
		}

		return new ResponseEntity<List<ProductResourceWithRelationships>>(productResourceList, HttpStatus.OK);
	}
	
	@GetMapping("/products-excludig-relationships")
	@ApiOperation(tags = { "Products" }, value = "Products excluding relationships", notes = "Get a list of all products without relationships.")
	@SuppressWarnings("unchecked")
	public ResponseEntity<List<ProductResource>> listProductsExcludingRelationships(
			@RequestParam(required = false) String name, Pageable pageable) throws Exception {

		List<Product> entities = (List<Product>) this.productService.list(name, pageable);

		List<ProductResource> simpleProductResourceList = (List<ProductResource>) Util.convertEntityListIntoResourceList(entities, ProductResource.class, Product.class );
		
		return new ResponseEntity<List<ProductResource>>(simpleProductResourceList, HttpStatus.OK);
	}
	
	@GetMapping("/products-excludig-relationships/{id}")
	@ApiOperation(tags = { "Products" }, value = "Products excluding relationships", notes = "Get a product without relationships.")
	public ResponseEntity<ProductResource> getProductExcludingRelationships(
			@RequestParam(required = false) Long id) throws Exception {

		Product entity = (Product) this.productService.find(Product.class, id);

		ProductResource simpleProductResource = ProductResource.fromEntity(entity);
		
		return new ResponseEntity<ProductResource>(simpleProductResource, HttpStatus.OK);
	}
	
	@PutMapping("/products/{id}")
	@ApiOperation(tags = { "Products" }, value = "Update product", notes = "Update a product information, including the parent product.") 
	public ResponseEntity<ProductResource> updateProduct(@PathVariable Long id, 
			@RequestBody ProductResource resource) throws Exception {
	
		Product entity = (Product) this.productService.find(Product.class, id);
	
		if(entity != null){
	
			entity.setName(resource.getName());
			entity.setDescription(resource.getDescription());
			
			Product parentProduct = null;
			
			if(resource.getParentProduct() != null && resource.getParentProduct().getId() != null) {
				parentProduct = (Product) this.productService.find(Product.class, resource.getParentProduct().getId());
			}
			
			entity.setParentProduct(parentProduct);
			
			this.productService.update(entity);
			
			return new ResponseEntity<ProductResource>(new ProductResource(entity), HttpStatus.OK); 
		}
	
		throw new Exception("No product found with the id '"+id+"'");
	
	}
	
	@DeleteMapping("/products/{id}")
	@ApiOperation(tags = { "Products" }, value = "Delete Product", notes = "Delete a product by it's id")
	@Transactional
	public ResponseEntity<ProductResourceWithRelationships> deleteProducts(@RequestParam(required = false) Long id) throws Exception {

		Product entity = (Product) this.productService.find(Product.class, id);

		if (entity == null || entity.getId() == null) {
			throw new Exception("No product found with the id '"+id+"'");
		}
		
		this.productService.delete(entity);
		
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
