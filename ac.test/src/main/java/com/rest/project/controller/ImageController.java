package com.rest.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.rest.project.domain.Image;
import com.rest.project.domain.Product;
import com.rest.project.dto.rest.ImageResource;
import com.rest.project.dto.rest.ProductResource;
import com.rest.project.service.ImageService;
import com.rest.project.service.ProductService;
import com.rest.project.util.Util;

import io.swagger.annotations.ApiOperation;

@Controller
public class ImageController {

	@Autowired
	private ImageService imageService;
	
	@Autowired
	private ProductService productService;
	
	@GetMapping("/products/{id}/images")
	@ApiOperation(tags = { "Images" }, value = "Product images list", notes = "Get a list of all images from a product.")
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public ResponseEntity<List<ImageResource>> listProductImages(@RequestParam(required = false) Long id,
			Pageable pageable) throws Exception {

		Product entity = (Product) this.productService.find(Product.class, id);
		
		if (entity == null || entity.getId() == null) {
			throw new Exception("No product found with the id '"+id+"'");
		}

		List<Image> images = (List<Image>) this.productService.listProductImages(id, pageable);

		List<ImageResource> productImages = (List<ImageResource>) Util.convertEntityListIntoResourceList(images, ImageResource.class, Image.class );

		return new ResponseEntity<List<ImageResource>>(productImages, HttpStatus.OK);
	}
	
	@PostMapping("/image")
	@ApiOperation(tags = { "Images" }, value = "Create image", notes = "Create a image.")
	public ResponseEntity<ImageResource> createProduct(@RequestBody ImageResource resource) throws Exception {
	
		Product product = (Product) this.productService.find(Product.class, resource.getProduct().getId());
	
		if(resource.getId() != null) {
			throw new Exception("To create a image, the ID must be empty.");
		}
		
		if(product == null || product.getId() == null) {
			throw new Exception("A product is needed to create an image.");
		}

		Image image = new Image(resource.getType(), ProductResource.toEntity(resource.getProduct()));
		
		this.imageService.create(image);
		
		return new ResponseEntity<ImageResource>(new ImageResource(image), HttpStatus.OK); 
	
	}
	
	@GetMapping("/images")
	@ApiOperation(tags = { "Images" }, value = "Image list", notes = "Get a list of all images and relationships. Relations: Parent Product")
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public ResponseEntity<List<ImageResource>> listImages(@RequestParam(required = false) String type,
			Pageable pageable) throws Exception {

		List<Image> entities = (List<Image>) this.imageService.list(type, pageable);

		List<ImageResource> imageResourceList = (List<ImageResource>) Util.convertEntityListIntoResourceList(entities, ImageResource.class, Image.class );

		return new ResponseEntity<List<ImageResource>>(imageResourceList, HttpStatus.OK);
	}
	
	@GetMapping("/images/{id}")
	@ApiOperation(tags = { "Images" }, value = "Get image", notes = "Get a image by it's id")
	@Transactional(readOnly = true)
	public ResponseEntity<ImageResource> getImage(@RequestParam(required = false) Long id) throws Exception {

		Image entity = (Image) this.imageService.find(Image.class, id);
		
		if (entity == null || entity.getId() == null) {
			throw new Exception("No image found with the id '"+id+"'");
		}

		ImageResource imageResource = ImageResource.fromEntity(entity);

		return new ResponseEntity<ImageResource>(imageResource, HttpStatus.OK);
	}

	@PutMapping("/images/{id}")
	@ApiOperation(tags = { "Images" }, value = "Update image", notes = "Update a image information, including the product that owns this image.") 
	public ResponseEntity<ImageResource> updateImage(@PathVariable Long id, 
			@RequestBody ImageResource resource) throws Exception {
	
		if(resource != null && (resource.getProduct() == null || resource.getProduct().getId() == null || resource.getProduct().getId().equals(0L))) {
			throw new Exception("To udate a image, the product who onws it must be informed.");
		}
		
		Image entity = (Image) this.imageService.find(Image.class, id);
	
		if(entity != null){
	
			entity.setType(resource.getType());
			
			Product parentProduct = null;
			
			if(resource.getProduct() != null && resource.getProduct().getId() != null) {
				parentProduct = (Product) this.productService.find(Product.class, resource.getProduct().getId());
			}
			
			entity.setProduct(parentProduct);
			
			this.imageService.update(entity);
			
			return new ResponseEntity<ImageResource>(new ImageResource(entity), HttpStatus.OK); 
		}
	
		throw new Exception("No image found with the id '"+id+"'");
	
	}
	
	@DeleteMapping("/images/{id}")
	@ApiOperation(tags = { "Images" }, value = "Delete Image", notes = "Delete a image by it's id")
	@Transactional
	public ResponseEntity<Image> deleteProducts(@RequestParam(required = false) Long id) throws Exception {

		Image entity = (Image) this.imageService.find(Image.class, id);

		if (entity == null || entity.getId() == null) {
			throw new Exception("No image found with the id '"+id+"'");
		}
		
		this.imageService.delete(entity);
		
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
}
