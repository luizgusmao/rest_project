package com.rest.project.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.rest.project.domain.Image;
import com.rest.project.repository.BusinessEntityRepository;
import com.rest.project.repository.ImageRepository;
import com.rest.project.service.ImageService;

@Repository
@Transactional(propagation = Propagation.MANDATORY, noRollbackFor = Exception.class)
public class ImageServiceImpl extends AbstractServiceImpl implements ImageService {

	@Autowired
	ImageRepository imageRepository;
	
	@Override
	public BusinessEntityRepository getBusinessEntityRepository() {
		return imageRepository;
	}
	
	@Override
	public List<Image> listProductImages(Long id, Pageable pageable) {
		
		List<Image> entitys = this.imageRepository.listProductImages(id, pageable);			
		
		return entitys;
	}

}
