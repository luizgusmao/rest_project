package com.rest.project.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.rest.project.domain.BusinessEntity;
import com.rest.project.domain.Image;
import com.rest.project.domain.Product;
import com.rest.project.repository.BusinessEntityRepository;
import com.rest.project.repository.ProductRepository;
import com.rest.project.service.ImageService;
import com.rest.project.service.ProductService;

@Repository
@Transactional(propagation = Propagation.MANDATORY, noRollbackFor = Exception.class)
public class ProductServiceImpl extends AbstractServiceImpl implements ProductService {

	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	ImageService imageService;
	
	@Override
	public BusinessEntityRepository getBusinessEntityRepository() {
		return productRepository;
	}

	@Override
	public List<Product> findByNameIgnoringCase(String name) {
		
		List<Product> entitys = this.productRepository.findByNameIgnoringCase(name);			
		
		return entitys;
	}
	
	@Override
	public List<Image> listProductImages(Long id, Pageable pageable) {
		
		List<Image> entitys = this.imageService.listProductImages(id, pageable);			
		
		return entitys;
	}
	
	@Override
	//Recursive method to delete all childs products. I don't like to use delete cascading
	public void delete(BusinessEntity entity) {
		
		if(entity instanceof Product) {
			Product product = (Product) entity;
			
			for (Product childProduct : product.getChildProducts()) {
				this.delete(childProduct);
			}
			
			this.productRepository.delete(product);
		}
		
	}

}
