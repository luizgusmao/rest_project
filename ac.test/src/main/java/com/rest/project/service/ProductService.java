package com.rest.project.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.rest.project.domain.Image;
import com.rest.project.domain.Product;

public interface ProductService extends BusinessEntityService{

	List<Product> findByNameIgnoringCase(String name);
	
	List<Image> listProductImages(Long id, Pageable pageable);
	
}
