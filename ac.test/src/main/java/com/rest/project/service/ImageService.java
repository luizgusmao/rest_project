package com.rest.project.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.rest.project.domain.Image;

public interface ImageService extends BusinessEntityService{

	List<Image> listProductImages(Long id, Pageable pageable);
	
}
