package com.rest.project.service.impl;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.rest.project.domain.BusinessEntity;
import com.rest.project.repository.BusinessEntityRepository;
import com.rest.project.service.BusinessEntityService;

@Repository
public class AbstractServiceImpl implements BusinessEntityService {
	
	@Override
	public void create(BusinessEntity object) {
		getBusinessEntityRepository().create(object);
	}

	@Override
	public void update(BusinessEntity object) {
		getBusinessEntityRepository().update(object);
	}

	@Override
	public BusinessEntity find(Class<?> clazz, Long id) {
		
		BusinessEntity entity = getBusinessEntityRepository().find(clazz, id);
		
		return entity;
	}

	@Override
	public List<BusinessEntity> list(String q, Pageable pageable) {
		
		List<BusinessEntity> entitys = getBusinessEntityRepository().list(q, pageable);
		
		return entitys;
	
	}

	@Override
	public void delete(BusinessEntity entity) {
		getBusinessEntityRepository().delete(entity);
	}

	@Override
	public BusinessEntityRepository getBusinessEntityRepository() {
		//Each child class must implement this method, returning his repository implementation
		return null;
	}
	
}
