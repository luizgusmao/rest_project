package com.rest.project.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.rest.project.domain.BusinessEntity;
import com.rest.project.repository.BusinessEntityRepository;

public interface BusinessEntityService {

	void create(BusinessEntity object);

	void update(BusinessEntity object);
	
	BusinessEntity find(Class<?> clazz, Long id);
	
	List<? extends BusinessEntity> list(String q, Pageable pageable);
	
	void delete(BusinessEntity id);
	
	BusinessEntityRepository getBusinessEntityRepository() throws Exception;
	
}
