package com.rest.project.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.rest.project.domain.BusinessEntity;

public interface BusinessEntityRepository {

	void create(BusinessEntity object);

	void update(BusinessEntity object);
	
	BusinessEntity find(Class<?> clazz, Long id);
	
	List<BusinessEntity> list(String q, Pageable pageable);
	
	void delete(BusinessEntity entity);
	
}
