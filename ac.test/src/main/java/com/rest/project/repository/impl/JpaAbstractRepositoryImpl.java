package com.rest.project.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.rest.project.domain.BusinessEntity;
import com.rest.project.repository.BusinessEntityRepository;

@Repository
public abstract class JpaAbstractRepositoryImpl implements BusinessEntityRepository {

	@PersistenceContext
	protected EntityManager entityManager;
	
	@Override
	@Transactional
	public void create(BusinessEntity object) {
		this.entityManager.persist(object);
	}

	@Override
	@Transactional
	public void update(BusinessEntity object) {
		this.entityManager.merge(object);
	}

	@Override
	public BusinessEntity find(Class<?> clazz, Long id) {
		
		BusinessEntity businessEntity = null;
		
		if(id != null) {
			businessEntity = (BusinessEntity) this.entityManager.find(clazz, id);
		}
		 
		return businessEntity;
	}
	
	@Override
	@Transactional
	public void delete(BusinessEntity entity) {
		this.entityManager.remove(entity);
	}
	
	protected void setPagination(TypedQuery<?> query, Pageable pageable) {
		query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
		query.setMaxResults(pageable.getPageSize());
	}

}
