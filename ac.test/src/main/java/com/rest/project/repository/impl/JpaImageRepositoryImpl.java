package com.rest.project.repository.impl;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.rest.project.domain.BusinessEntity;
import com.rest.project.domain.Image;
import com.rest.project.repository.ImageRepository;

@Repository
@SuppressWarnings("unchecked")
public class JpaImageRepositoryImpl extends JpaAbstractRepositoryImpl implements ImageRepository {
	
	@Override
	public List<BusinessEntity> list(String q, Pageable pageable) {
		
		StringBuilder hql = new StringBuilder();
		
		hql.append(" Select image ");
		hql.append("   From Image image");
		
		if (q != null) {
			hql.append(" Where Upper(image.type) LIKE :q ");
		}
		
		hql.append(" Order By image.type ASC ");

		TypedQuery<?> query = this.entityManager.createQuery(hql.toString(), Image.class);
		
		if(q != null){
			query.setParameter("q", q.toUpperCase() + "%");
		}
		
		if(pageable != null){
			setPagination(query, pageable);
		}
		
		return (List<BusinessEntity>) query.getResultList();
	}
	
	@Override
	public List<Image> listProductImages(Long id, Pageable pageable) {
		
		StringBuilder hql = new StringBuilder();
		
		hql.append(" Select image ");
		hql.append("   From Image image");
		hql.append("  Where image.product.id = :id ");
		hql.append("  Order By image.type ASC ");

		TypedQuery<?> query = this.entityManager.createQuery(hql.toString(), Image.class);
		
		if(id != null){
			query.setParameter("id", id);
		}
		
		if(pageable != null){
			setPagination(query, pageable);
		}
		
		return (List<Image>) query.getResultList();
	}

}
