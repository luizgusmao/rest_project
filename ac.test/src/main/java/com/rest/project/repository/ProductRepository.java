package com.rest.project.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.rest.project.domain.Product;

public interface ProductRepository extends BusinessEntityRepository {

	List<Product> listChildProducts(Long id, Pageable pageable);
	
	List<Product> findByNameIgnoringCase(String name);
	
}
