package com.rest.project.repository.impl;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.rest.project.domain.BusinessEntity;
import com.rest.project.domain.Product;
import com.rest.project.repository.ProductRepository;

@Repository
@SuppressWarnings("unchecked")
public class JpaProductRepositoryImpl extends JpaAbstractRepositoryImpl implements ProductRepository {
	
	@Override
	public List<BusinessEntity> list(String q, Pageable pageable) {
		
		StringBuilder hql = new StringBuilder();
		
		hql.append(" Select product ");
		hql.append("   From Product product");
		
		if (q != null) {
			hql.append(" Where Upper(product.name) LIKE :q ");
		}
		
		hql.append(" Order By product.name ASC ");

		TypedQuery<?> query = this.entityManager.createQuery(hql.toString(), Product.class);
		
		if(q != null){
			query.setParameter("q", q.toUpperCase() + "%");
		}
		
		if(pageable != null){
			setPagination(query, pageable);
		}
		
		return (List<BusinessEntity>) query.getResultList();
	}
	
	@Override
	public List<Product> listChildProducts(Long id, Pageable pageable) {
		
		StringBuilder hql = new StringBuilder();
		
		hql.append(" Select product ");
		hql.append("   From Product product");
		hql.append("  Where product.parentProduct.id = :id ");
		hql.append("  Order By product.name ASC ");

		TypedQuery<?> query = this.entityManager.createQuery(hql.toString(), Product.class);
		
		if(id != null){
			query.setParameter("id", id);
		}
		
		if(pageable != null){
			setPagination(query, pageable);
		}
		
		return (List<Product>) query.getResultList();
	}

	@Override
	public List<Product> findByNameIgnoringCase(String name) {
		
		StringBuilder hql = new StringBuilder();
		
		hql.append(" Select product ");
		hql.append("   From Product product");
		hql.append("  Where Upper(product.name) = :name ");
		hql.append("  Order By product.name ASC ");

		TypedQuery<?> query = this.entityManager.createQuery(hql.toString(), Product.class);
			
		query.setParameter("name", name.toUpperCase());
		
		return (List<Product>) query.getResultList();

	}
	
	@Override
	@Transactional
	public void delete(BusinessEntity entity) {
			
		StringBuilder hqlImages = new StringBuilder();
		
		hqlImages.append(" Delete Image image ");
		hqlImages.append("  Where image.product.id = :id ");

		Query queryImages = this.entityManager.createQuery(hqlImages.toString());
			
		queryImages.setParameter("id", ((Product) entity).getId());
		
		queryImages.executeUpdate();
		
		this.entityManager.remove(entity);
	}

}
