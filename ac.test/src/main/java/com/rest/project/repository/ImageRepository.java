package com.rest.project.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.rest.project.domain.Image;

public interface ImageRepository extends BusinessEntityRepository {

	public List<Image> listProductImages(Long id, Pageable pageable);
	
}
