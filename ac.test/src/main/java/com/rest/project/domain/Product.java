package com.rest.project.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;


@Entity
public class Product extends BusinessEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "product_generator", sequenceName = "product_sequence", initialValue = 1)
	@GeneratedValue(generator = "product_generator")
	@Column(name = "ID")
	private Long id;

	@Column(nullable = false)
	private String name;
	
	@Column(nullable = true)
	private String description;

	@OneToMany(mappedBy = "product", targetEntity = Image.class, fetch = FetchType.LAZY)
	private List<Image> images;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PARENT_PRODUCT_ID", referencedColumnName = "ID")
	private Product parentProduct;
	
	@OneToMany(mappedBy = "parentProduct", targetEntity = Product.class, fetch = FetchType.LAZY)
	private List<Product> childProducts;
	
	public Product() {
	}
	
	public Product(Long id) {
		this.setId(id);
	}

	public Product(String name) {
		this.name = name;
	}
	
	public Product(String name, String description, Product parentProduct) {
		this.name = name;
		this.description = description;
		this.parentProduct = parentProduct;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

	public Product getParentProduct() {
		return parentProduct;
	}

	public void setParentProduct(Product parentProduct) {
		this.parentProduct = parentProduct;
	}

	public List<Product> getChildProducts() {
		return childProducts;
	}

	public void setChildProducts(List<Product> childProducts) {
		this.childProducts = childProducts;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", description=" + description + ", images=" + images
				+ ", parentProduct=" + parentProduct + ", childProducts=" + childProducts + "]";
	}



}
