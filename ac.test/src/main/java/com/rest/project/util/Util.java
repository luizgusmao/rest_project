package com.rest.project.util;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class Util {

	private static final String FROM_ENTITY_METHOD = "fromEntity";
	
	private static final String TO_ENTITY_METHOD = "fromEntity";
	
	public static List<?> convertResourceListIntoEntityList(List<?> list, Class<?> resourceClass, Class<?> entityClass) throws Exception {
		return convertList(list, resourceClass, entityClass, TO_ENTITY_METHOD);
	}
	
	public static List<?> convertEntityListIntoResourceList(List<?> list, Class<?> resourceClass, Class<?> entityClass) throws Exception {
		return convertList(list, resourceClass, entityClass, FROM_ENTITY_METHOD);
	}
	
	private static List<?> convertList(List<?> sourceList, Class<?> resourceClass, Class<?> entityClass, String methodName) throws Exception {

		List<Object> result = null;

        if (sourceList != null && !sourceList.isEmpty() && resourceClass != null) {

        	result = new ArrayList<Object>();

            for (Object object : sourceList) {

                Method method = resourceClass.getMethod(methodName, entityClass);

                if (Modifier.isStatic(method.getModifiers())) {

                    Object objectRetorno = method.invoke(null, object);

                    if (objectRetorno != null && resourceClass.equals(objectRetorno.getClass())) {
                    	result.add(objectRetorno);
                    }

                }
            }
        }

        return result;
	 }

}
