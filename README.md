# Project Title

This project was built to show the implementation of rest services and spring boot usage

## Getting Started

To compile the project you will need to use the command below, on the folder ac.test
mvn clean install

## Running the tests

At the moment this project has no test implemented. I had no time to implement them yet.

## Deployment

To deploy the project you will need to use the command below, on the folder ac.test
mvn spring-boot:run

It will run spring boot web application with rest services and a Swagger interface with documentation and test for rest services.

I implemented the swagger application to access the rest interface, to access the interface you must start the service and access http://localhost:8080/swagger-ui.html, there you will find the Images and Products rest's services and documentation.

There will have all examples for each call of rest services.